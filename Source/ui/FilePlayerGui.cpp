/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"
#define NUM_HRZNTL_ELEMS 3

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)

{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    positionSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    positionSlider.setRange(0.0, 1.0);
    positionSlider.addListener(this);
    addAndMakeVisible(&positionSlider);
    
    pitchSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    pitchSlider.addListener(this);
    addAndMakeVisible(&pitchSlider);
}

FilePlayerGui::~FilePlayerGui()
{
    
}

//Component
void FilePlayerGui::resized()
{
    int hightByElems = getHeight()/NUM_HRZNTL_ELEMS;
    
    positionSlider.setBounds(0, 0, getWidth(), hightByElems);
    playButton.setBounds (0, hightByElems, hightByElems, hightByElems);
    fileChooser->setBounds (hightByElems, hightByElems, getWidth()-hightByElems, hightByElems);
    pitchSlider.setBounds(0, hightByElems * (NUM_HRZNTL_ELEMS - 1), getWidth(), hightByElems);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        
        filePlayer.setPlaying(!filePlayer.isPlaying());
        
        if (filePlayer.isPlaying())
        {
            startTimer(250);
        }
        else if (!filePlayer.isPlaying())
        {
            stopTimer();
        }
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

// Slider Listener
void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &positionSlider)
    {
        filePlayer.setPosition(positionSlider.getValue());
    }
    else if (slider == &pitchSlider)
    {
        filePlayer.setPlaybackRate(pitchSlider.getValue());
    }
}

void FilePlayerGui::timerCallback()
{
    positionSlider.setValue(filePlayer.getPosition());
}



